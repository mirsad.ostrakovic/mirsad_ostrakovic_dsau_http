#include "string.h"
#include "stdio.h"
#include "arpa/inet.h"
#include "sys/socket.h"
#include "unistd.h"
#include "pthread.h"
#include "stdlib.h"
#include "stdbool.h"
#include "netdb.h"

#include "util.h"


#define ALERT(a) { printf(a); }
#define ERROR(a, b) { printf(a); return b; }

#define HTTP_REQUEST_DEFAULT_PAGE "GET / HTTP/1.0\r\n"
#define HTTP_REQUEST_PREFIX "GET "
#define HTTP_REQUEST_POSTFIX " HTTP/1.0\r\n"

static int connect_to_server(struct in_addr serverAddress, int serverPort)
{
  int socketFD;
  struct sockaddr_in serverAddr;

  if((socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    ERROR("connect_to_server(): can not get socket\n", -1);

  memset(&serverAddr, 0, sizeof(struct sockaddr_in));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(serverPort);
  serverAddr.sin_addr.s_addr = serverAddress.s_addr;
  
  if(connect(socketFD, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in)) < 0)
    ERROR("connect_to_server(): can not reach server\n", -2);

  return socketFD;
}



static bool getServerResource(int socketFD, const char *resourcePath, const char *filePath)
{  
  char buffer[4096];
  size_t currentLen;
  int pos;
  size_t endPos;

  FILE *file;

  bool isResponseHeaderRecv = false;

  if(resourcePath == NULL)
  {
    pos = strlen(HTTP_REQUEST_DEFAULT_PAGE);
    memcpy(buffer, HTTP_REQUEST_DEFAULT_PAGE, pos);
  }
  else
  {
    pos = strlen(HTTP_REQUEST_PREFIX);
    memcpy(buffer, HTTP_REQUEST_PREFIX, pos);

    memcpy(&buffer[pos], resourcePath, strlen(resourcePath));
    pos += strlen(resourcePath);

    memcpy(&buffer, HTTP_REQUEST_POSTFIX, strlen(HTTP_REQUEST_POSTFIX));
    pos += strlen(HTTP_REQUEST_POSTFIX);
  }

  buffer[pos] = '\r'; buffer[pos+1] = '\n';
  pos += 2;

  if(!sendContent(socketFD, buffer, pos))
    ERROR("getServerResource(): sendContent() can not send HTTP request message\n", -1)


  pos = 0;
  while(!isResponseHeaderRecv){
   
    if((currentLen = recv(socketFD, &buffer[pos], sizeof(buffer) - pos, 0)) <= 0)
      ERROR("getServerResource(): can not read from socket and message is not compleated at all\n", -1)
    
    for(int i = (pos - 3 < 0) ? 0 : pos - 3; i < pos + currentLen - 3 ; ++i)
      if(buffer[i] == '\r' && buffer[i+1] == '\n' && buffer[i+2] == '\r' && buffer[i+3] == '\n')
      {
        if(i + 4 != pos + currentLen)
        {
          ALERT("getServerResourcet(): read message till end of header, but other data also came\n"); 
          endPos = pos + currentLen;
          pos = i + 4;
        }
        else
          endPos = pos += currentLen;
        isResponseHeaderRecv = true;
        break;
      }

    if(isResponseHeaderRecv) break;
    
    pos += currentLen; 
  }

  if((file = fopen(filePath, "w+")) == NULL)
    ERROR("getServerResource(): can not open file\n", -1)

  if((pos != endPos) && (fwrite(&buffer[pos], 1u, endPos - pos, file) != (endPos - pos)) && ferror(file))
  {
    fclose(file);
    ERROR("getServerResource(): error in receiving server data immediate after header\n", -1)
  }

  while(true)
  {   
    currentLen = recv(socketFD, buffer, sizeof(buffer), 0);

    if(currentLen < 0)
      ERROR("getServerResource(): error in receiving server response\n", -1)

    if(currentLen == 0)
      break;
    
    if(fwrite(buffer, 1u, currentLen, file) != currentLen && ferror(file))
    {
      fclose(file);
      ERROR("getServerResource(): problem with writing data in file\n", -1)
    }
  
  }
  
  fclose(file);
}


bool getNumFromString(int *num, const char *str)
{
  char *endPtr;
 
  *num = strtol(str, &endPtr, 10);

  if(str + strlen(str) != endPtr)
    return false;
  
  return true;
}



int main(int argc, char *argv[])
{
  char *url;
  char *filePath;
  char *resourcePath;
  
  struct hostent *serverInfo;
  struct in_addr serverAddr;
  int socketFD;
  int port;

  if(argc == 2)
  {
    url = argv[1]; 
    port = 80;
    filePath = "server_response.txt";
    resourcePath = NULL;
  }
  else if(argc == 3){ 
    url = argv[1];
    if(!getNumFromString(&port, argv[2]))
      ERROR("main(): getNumFromString() report invalid port number\n", -1)
    filePath = "server_response.txt";
    resourcePath = NULL;
  }
  else if(argc == 4)
  {
    url = argv[1]; 
    if(!getNumFromString(&port, argv[2]))
      ERROR("main(): getNumFromString() report invalid port number\n", -1)
    filePath = argv[3];
    resourcePath = NULL;
  }  
  else if(argc == 5)
  {
    url = argv[1];
    if(!getNumFromString(&port, argv[2]))
      ERROR("main(): getNumFromString() report invalid port number\n", -1)
    filePath = argv[3];
    resourcePath = argv[4];
  }
  else
    ERROR("main(): invalid number of args\n", -1)



  if((serverInfo = gethostbyname(url)) == NULL)
    ERROR("main(): gethostbyname() can not get server address\n", -1)

  if(serverInfo->h_addr_list[0] == NULL)
    ERROR("main(): invalid server URL/IP\n", -1)

  serverAddr = *((struct in_addr *)(serverInfo->h_addr_list[0]));

  if((socketFD = connect_to_server(serverAddr, 80)) < 0)
    ERROR("main(): connect_to_server() can not connect to server\n", -1)

  getServerResource(socketFD, resourcePath, filePath); 

  close(socketFD);

  return 0;
}
