#include "util.h"


bool recvContent(int socketFD, void* buffer, size_t msgLength){

  size_t readLen = 0u;
  size_t currentLen;

  while(true){ 
    
    if((currentLen = recv(socketFD, &((char *)(buffer))[readLen], msgLength - readLen, 0)) <= 0)
      return false;

    readLen += currentLen;
    
    if( readLen == msgLength )
      return true;
  }
}


bool sendContent(int socketFD, void* buffer, size_t msgLength){

  size_t sendLen = 0u;
  size_t currentLen;

  while(true){ 
    
    if((currentLen = send(socketFD, &((char *)(buffer))[sendLen], msgLength - sendLen, 0)) <= 0)
      return false;
    
    sendLen += currentLen;

    if( sendLen == msgLength )
      return true;
  }
}

