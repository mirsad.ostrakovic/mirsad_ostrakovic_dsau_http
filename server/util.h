#ifndef UTIL_H
#define UTIL_H


#include "stdbool.h"
#include "arpa/inet.h"
#include "sys/socket.h"
#include "unistd.h"


bool recvContent(int socketFD, void* buffer, size_t msgLength);
bool sendContent(int socketFD, void* buffer, size_t msgLength);

#endif
