#ifndef CLIENT_SERVICE_H
#define CLIENT_SERVICE_H

#include "stdio.h"
#include "arpa/inet.h"
#include "sys/socket.h"
#include "unistd.h"
#include "stdlib.h"
#include "stdbool.h"
#include "string.h"
#include "fcntl.h"

#include "util.h"

typedef struct{
  int socketFD;
} client_service_args;

//void * client_service(void *args);
void client(int socketFD);
void * clientService(void *args);

#endif
