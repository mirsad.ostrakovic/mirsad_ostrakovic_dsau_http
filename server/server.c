#include "string.h"
#include "stdio.h"
#include "arpa/inet.h"
#include "sys/socket.h"
#include "unistd.h"
#include "pthread.h"
#include "stdlib.h"
#include "stdbool.h"

#include "client_service.h"

#define HTTP_SERVER_PORT 8080
#define LISTEN_BACKLOG 10
#define ERROR(a, b) { printf(a); return b; }
#define STATUS(a) { printf(a); }


static int start_http_server()
{
  int socketFD;
  struct sockaddr_in serverAddr;

  if((socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    ERROR("start_http_server(): can not get socket\n", -1);

  memset(&serverAddr, 0, sizeof(struct sockaddr_in));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(HTTP_SERVER_PORT);
  serverAddr.sin_addr.s_addr = INADDR_ANY;

  if(bind(socketFD, (struct sockaddr *)&serverAddr, sizeof(struct sockaddr_in)) < 0)
    ERROR("start_http_server(): can not bind to port\n", -2);

  if(listen(socketFD, LISTEN_BACKLOG) < 0)
    ERROR("start_http_server(): can not listen\n", -3);

  return socketFD;
}



static int start_client_thread(int clientSocket)
{
  pthread_t threadID;
  pthread_attr_t threadAttr;
  
  client_service_args *args = (client_service_args *)malloc(sizeof(client_service_args));
  args->socketFD = clientSocket;

  if(pthread_attr_init(&threadAttr) != 0)
    ERROR("start_client_thread(): problem with pthread_attr_init()\n", -1);

  if(pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_DETACHED) != 0)
    ERROR("start_client_thread(): problem with pthread_attr_setdetachstate()\n", -2);

  if(pthread_create(&threadID, &threadAttr, clientService, args) != 0)
    ERROR("start_client_thread(): problem with pthread_create()\n", -3);

  if(pthread_attr_destroy(&threadAttr) != 0)
    ERROR("start_client_thread(): problem with pthread_attr_destroy()\n", -4);

  return 0;
}





int main(int argc, char *argv[])
{
  int serverSocket;
  int clientSocket;

  if((serverSocket = start_http_server()) < 0)
    ERROR("main(): can not get server socket\n", -1);

  while(true){
    if((clientSocket = accept(serverSocket, NULL, NULL)) < 0)
      ERROR("main(): accept return invalid socket file descriptor\n", -1);
    
    STATUS("main(): connection accepted\n")

    if(start_client_thread(clientSocket) < 0)
      ERROR("main(): start_client_thread() can not start thread\n", -1);
  }

  return 0;
}
