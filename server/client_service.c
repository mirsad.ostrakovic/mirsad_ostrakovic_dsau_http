#include "client_service.h"


#define ERROR(a) { printf(a); return; }
#define ALERT(a) { printf(a); }

#define HTTP_STATUS_LINE_200 "HTTP/1.0 200 OK\r\n"
#define HTTP_STATUS_LINE_404 "HTTP/1.0 404 Not Found\r\n"





static void sendResource(int socketFD, const char* path)
{
  FILE *file; 
  char buffer[2048];
  int pos;
  int state = 0;


  while(true)
  {
    switch(state)
    {
      case 0:
        if((file = fopen(path[0] == '/' ? &path[1] : path, "r")) == NULL)
          state = 100;
        else
          state = 1;
        break;



      case 1:
        pos = strlen(HTTP_STATUS_LINE_200);
        memcpy(buffer, HTTP_STATUS_LINE_200, pos);
        buffer[pos] = '\r'; buffer[pos + 1] = '\n';
        pos += 2;

        if(sendContent(socketFD, buffer, pos))
          state = 2;
        else 
          state = 50;
        break;


      case 2:
        while(true)
        {
          pos = fread(buffer, 1, sizeof(buffer), file);
    
          if(pos != sizeof(buffer))
          {
            if(ferror(file))
              state = 51;
            else if(feof(file))
              state = 3;
            else
              state = 53;
            break;     
          }
 
          if(!sendContent(socketFD, buffer, sizeof(buffer))){
            state = 52;
            break;
          }
        }
      
      case 3:
        if(sendContent(socketFD, buffer, pos))
          state = 4;
        else
          state = 52;
        break;


      case 4:
        printf("sendResource(): FILE SUCCESSFULLY SENT\n");
        return;

      case 50:
        ERROR("sendResource(): send_content() has problem with sending 200 OK response header\n")

      case 51:
        ERROR("sendResource(): error in reading file\n"); 

      case 52:
        ERROR("sendResource(): send_content() can not send data\n");

      case 53:
        ERROR("sendResource(): unknown error\n");


      case 100:
        pos = strlen(HTTP_STATUS_LINE_404);
        memcpy(buffer, HTTP_STATUS_LINE_404, pos);
        buffer[pos] = '\r'; buffer[pos + 1] = '\n';
        pos += 2;

        if(!sendContent(socketFD, buffer, pos))
          state = 150;
        else
          state = 101;
        break;

      case 101:
        ERROR("sendResource(): error in open file\n")

      case 150:
        ERROR("sendResource(): send_content() has problem with sending 404 not found\n");
    }
  }
}







void client(int socketFD)
{
  char buffer[2048];
  int readLen = 0u;
  int currentLen;
  int isReqeustRecv = 0;
  
  
  char *pathBegin;
  char *pathEnd;
  char path[256];

  int state = 0;
  int isEndStateMachine = 0;
  
  // RECEIVE REQUEST
  // ---------------
  while(!isReqeustRecv){
    
    if((currentLen = recv(socketFD, &buffer[readLen], sizeof(buffer) - readLen, 0)) <= 0)
      ERROR("client(): can not read from socket and message is not compleated at all\n")
  
    for(int i = (readLen - 3 < 0) ? 0 : readLen - 3; i < readLen + currentLen - 3 ; ++i)
      if(buffer[i] == '\r' && buffer[i+1] == '\n' && buffer[i+2] == '\r' && buffer[i+3] == '\n')
      {
        if(i + 4 != readLen + currentLen)
        {
          ALERT("client(): read message till end of header, but other data also came\n"); 
          readLen = i + 4;
        }
        else
          readLen += currentLen;
        isReqeustRecv = 1; 
        break;
      }

    if(isReqeustRecv) break;
    
    readLen += currentLen; 
  }


  // PROCESS REQUEST
  // ---------------
  currentLen = 0;

  while(!isEndStateMachine){
    switch(state){
      
      case 0:
        if(memcmp(&buffer[currentLen], "GET", 3) == 0){
          currentLen += 3;
          state = 1;
        }
        else 
          state = 98;
        break;


      case 1:
        if(buffer[currentLen++] == ' ')
          state = 2;
        else
          state = 99;
        break;


      case 2:
        if(buffer[currentLen] == ' ' || buffer[currentLen] == '\r' || buffer[currentLen] == '\n')
          state = 99;
        else{
          state = 3;
          pathBegin = &buffer[currentLen];
        }
        ++currentLen;
        break;


      case 3:
        if(buffer[currentLen] == ' '){
          state = 4;
          pathEnd = &buffer[currentLen];
        }
        else if(buffer[currentLen] == '\r' || buffer[currentLen] == '\n')
          state = 99;
        ++currentLen;
        break;


      case 4:
        if(memcmp(&buffer[currentLen], "HTTP/1.", 7) == 0){
          currentLen += 7;
          state = 5;
        }
        else 
          state = 99;
      break;


      case 5:
        if(buffer[currentLen] == '0' || buffer[currentLen] == '1')
          state = 6;
        else
          state = 99;
        ++currentLen;
        break;


      case 6:
        if(buffer[currentLen++] == '\r' && buffer[currentLen++] == '\n')
          state = 7;
        else
          state = 99;
        break;


      case 7:
        isEndStateMachine = true; 
        break;


      case 98:
        ERROR("client(): server process only GET HTTP reqeuest\n")

      case 99:
        ERROR("client(): invalid HTTP request\n") 
    }
  }

 
  //CHECK IF PATH IS DEFAULT
  //-----------------------
  memcpy(path, pathBegin, pathEnd - pathBegin);
  if(strcmp(path, "/") == 0){
    memcpy(&path[1], "index.html", 10);
    path[11] = '\0';
  }

  //SEND RESOURCE ON PATH
  //---------------------
  sendResource(socketFD, path);

}




void * clientService(void *args){
  int socketFD = ((client_service_args *)(args))->socketFD;
  free(args);
  client(socketFD);
  close(socketFD);
  return NULL;
}







//other version of implementation






/*

void send_resource(int socketFD, const char* path)
{
  FILE *file;
  
  char buffer[2048];
  int pos;

  printf("path ->%s<-\n", path);


  if((file = fopen(path[0] == '/' ? &path[1] : path, "r")) == NULL)
  {

    pos = strlen(HTTP_STATUS_LINE_404);
    memcpy(buffer, HTTP_STATUS_LINE_404, pos);
    buffer[pos] = '\r'; buffer[pos + 1] = '\n';
    pos += 2;

    if(!send_content(socketFD, buffer, pos))
      ERROR("send_file(): send_content() has problem with sending 404 not found");

    ERROR("send_file(): error in open file")
  }

  printf("file opened\n");
 
  pos = strlen(HTTP_STATUS_LINE_200);
  memcpy(buffer, HTTP_STATUS_LINE_200, pos);
  buffer[pos] = '\r'; buffer[pos + 1] = '\n';
  pos += 2;

  if(!send_content(socketFD, buffer, pos))
    ERROR("send_file(): send_content() has problem with sending header");

  while(true)
  {
    pos = fread(buffer, 1, sizeof(buffer), file);
    
    if(pos != sizeof(buffer))
    {
      if(ferror(file))
        ERROR("send_file(): error in reading file"); 
      if(feof(file))
      {
        if(!send_content(socketFD, buffer, pos))
          ERROR("send_file(): send_content() can not send data");
        printf("EOF reached\n");
        break;
      }
      ERROR("send_file(): unknown error");
    }
 
    if(!send_content(socketFD, buffer, sizeof(buffer)))
      ERROR("send_file(): send_content() can not send data");
    //printf("%.*s", readLen, buffer);
  }

}
*/

